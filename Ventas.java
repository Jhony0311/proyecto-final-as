package restaurante;
import java.util.Scanner;
public class Ventas {
	private Scanner sc = new Scanner (System.in);
	private double ventasSemanales;
	private double ventasSemanalesPlatillo [] = new double [7];
	private int cVP [] = {0, 0, 0, 0, 0, 0, 0}; // Cantidad Vendida Por Platillo Total
	private double ventasDiarias [] = {0, 0, 0, 0, 0, 0, 0};
	private double jubiladoDiario [] = new double [7];
	private double ofertaDiario [] = new double [7];

	public void guardarVentaPlatillo(int platillo, double ventaSub, int cantidad, double descuento, String Dia, int encargado) {
		ventasSemanalesPlatillo[platillo - 1] = ventasSemanalesPlatillo[platillo - 1] + (venta - descuento);
		cVP[platillo - 1] = cVP[platillo - 1] + cantidad;

		if (dia == "lunes" && descuento > 0) {
			ofertaDiario[0] = ofertaDiario[0] + descuento;
		} else if (dia == "martes" && descuento > 0) {
			ofertaDiario[2] = ofertaDiario[2] + descuento;
		} else if (dia == "miercoles" && descuento > 0) {
			ofertaDiario[3] = ofertaDiario[3] + descuento;
		} else if (dia == "jueves" && descuento > 0) {
			ofertaDiario[4] = ofertaDiario[4] + descuento;
		} else if (dia == "viernes" && descuento > 0) {
			ofertaDiario[5] = ofertaDiario[5] + descuento;
		} else if (dia == "sabado" && descuento > 0) {
			ofertaDiario[6] = ofertaDiario[6] + descuento;
		} else if (dia == "domingo" && descuento > 0) {
			ofertaDiario[7] = ofertaDiario[7] + descuento;
		}
	}
	public void guardarDescuentoJubilado(double descuento, String dia) {
		if (dia == "lunes") {
			jubiladoDiario[0] = jubiladoDiario[0] + descuento;
		} else if (dia == "martes") {
			jubiladoDiario[2] = jubiladoDiario[2] + descuento;
		} else if (dia == "miercoles") {
			jubiladoDiario[3] = jubiladoDiario[3] + descuento;
		} else if (dia == "jueves") {
			jubiladoDiario[4] = jubiladoDiario[4] + descuento;
		} else if (dia == "viernes") {
			jubiladoDiario[5] = jubiladoDiario[5] + descuento;
		} else if (dia == "sabado") {
			jubiladoDiario[6] = jubiladoDiario[6] + descuento;
		} else if (dia == "domingo") {
			jubiladoDiario[7] = jubiladoDiario[7] + descuento;
		}
	}
	public void guardarVentaDiaria(total, dia, encargado) {
		if (dia == "lunes") {
			ventasDiarias[0] = ventasDiarias[0] + total;
		} else if (dia == "martes") {
			ventasDiarias[1] = ventasDiarias[1] + total;
		} else if (dia == "miercoles") {
			ventasDiarias[2] = ventasDiarias[2] + total;
		} else if (dia == "jueves") {
			ventasDiarias[3] = ventasDiarias[3] + total;
		} else if (dia == "viernes") {
			ventasDiarias[4] = ventasDiarias[4] + total;
		} else if (dia == "sabado") {
			ventasDiarias[5] = ventasDiarias[5] + total;
		} else if (dia == "domingo") {
			ventasDiarias[6] = ventasDiarias[6] + total;
		}
	}
	public void Semanal() {
		System.out.println("las ventas de la semana fueron de la siguiente manera");
		for (int i = 0; i < ventasSemanalesPlatillo.length; i++) {
			System.out.println("- Platillo " + (i+1) + ": "  + ventasSemanalesPlatillo[i]);
		}
	}
	public void Diarias() {
		// TODO: salvar ventas diarias de cada platillo en guardarVentaPlatillo
		
	}
	public void DiariasPorEncargado() {
		// TODO: salvar ventas diarias por encargado en guardarVentaDiaria
		
	}
	public void PorDistribucionPorcentual() {
		for (int j = 0; j < ventasSemanalesPlatillo.length; j++) {
			double porcentaje = (ventasSemanalesPlatillo[j] / ventasSemanales) * 100;
			System.out.println("El platillo " + (j+1) + "representa el " + porcentaje + "% de las ventas semanales");
		}
	}
	public void MasVendido() {
		if (cVP[0] > cVP[1] && cVP[0] > cVP[2] && cVP[0] > cVP[3] && cVP[0] > cVP[4] && cVP[0] > cVP[5] && cVP[0] > cVP[6]  ) {
			System.out.println("El platillo mas vendido de la semana es el platillo 1");
		}
		
		if (cVP[1] > cVP[0] && cVP[1] > cVP[2] && cVP[1] > cVP[3] && cVP[1] > cVP[4] && cVP[1] > cVP[5] && cVP[1] > cVP[6]  ) {
			System.out.println("El platillo mas vendido de la semana es el platillo 2");
		}
		
		if (cVP[2] > cVP[1] && cVP[2] > cVP[0] && cVP[2] > cVP[3] && cVP[2] > cVP[4] && cVP[2] > cVP[5] && cVP[2] > cVP[6]  ) {
			System.out.println("El platillo mas vendido de la semana es el platillo 3");
		}
		
		if (cVP[3] > cVP[1] && cVP[3] > cVP[2] && cVP[3] > cVP[0] && cVP[3] > cVP[4] && cVP[3] > cVP[5] && cVP[3] > cVP[6]  ) {
			System.out.println("El platillo mas vendido de la semana es el platillo 4");
		}
		
		if (cVP[4] > cVP[1] && cVP[4] > cVP[2] && cVP[4] > cVP[3] && cVP[4] > cVP[0] && cVP[4] > cVP[5] && cVP[4] > cVP[6]  ) {
			System.out.println("El platillo mas vendido de la semana es el platillo 5");
		}
		
		if (cVP[5] > cVP[1] && cVP[5] > cVP[2] && cVP[5] > cVP[3] && cVP[5] > cVP[4] && cVP[5] > cVP[0] && cVP[5] > cVP[6]  ) {
			System.out.println("El platillo mas vendido de la semana es el platillo 6");
		}
		
		if (cVP[6] > cVP[1] && cVP[6] > cVP[2] && cVP[6] > cVP[3] && cVP[6] > cVP[4] && cVP[6] > cVP[5] && cVP[6] > cVP[0]  ) {
			System.out.println("El platillo mas vendido de la semana es el platillo 7");
		}
	}
	public void MenosVendido() {
		if (cVP[0] < cVP[1] && cVP[0] < cVP[2] && cVP[0] < cVP[3] && cVP[0] < cVP[4] && cVP[0] < cVP[5] && cVP[0] < cVP[6]  ) {
			System.out.println("El platillo menos vendido de la semana es el platillo 1");
		}
		
		if (cVP[1] < cVP[0] && cVP[1] < cVP[2] && cVP[1] < cVP[3] && cVP[1] < cVP[4] && cVP[1] < cVP[5] && cVP[1] < cVP[6]  ) {
			System.out.println("El platillo menos vendido de la semana es el platillo 2");
		}
		
		if (cVP[2] < cVP[1] && cVP[2] < cVP[0] && cVP[2] < cVP[3] && cVP[2] < cVP[4] && cVP[2] < cVP[5] && cVP[2] < cVP[6]  ) {
			System.out.println("El platillo menos vendido de la semana es el platillo 3");
		}
		
		if (cVP[3] < cVP[1] && cVP[3] < cVP[2] && cVP[3] < cVP[0] && cVP[3] < cVP[4] && cVP[3] < cVP[5] && cVP[3] < cVP[6]  ) {
			System.out.println("El platillo menos vendido de la semana es el platillo 4");
		}
		
		if (cVP[4] < cVP[1] && cVP[4] < cVP[2] && cVP[4] < cVP[3] && cVP[4] < cVP[0] && cVP[4] < cVP[5] && cVP[4] < cVP[6]  ) {
			System.out.println("El platillo menos vendido de la semana es el platillo 5");
		}
		
		if (cVP[5] < cVP[1] && cVP[5] < cVP[2] && cVP[5] < cVP[3] && cVP[5] < cVP[4] && cVP[5] < cVP[0] && cVP[5] < cVP[6]  ) {
			System.out.println("El platillo menos vendido de la semana es el platillo 6");
		}
		
		if (cVP[6] < cVP[1] && cVP[6] < cVP[2] && cVP[6] < cVP[3] && cVP[6] < cVP[4] && cVP[6] < cVP[5] && cVP[6] < cVP[0]  ) {
			System.out.println("El platillo menos vendido de la semana es el platillo 7");
		}
	}
	public void DiaEspecifico() {
		Scanner sc = new Scanner (System.in);
		dia = sc.nextLine();

		dia = dia.toLowerCase();
		
		switch (dia) {
			case "lunes":
				System.out.println("Las ventas del lunes fueron por: " + ventasDiarias[0]);
				break;
			case "martes":
				System.out.println("Las ventas del martes fueron por: " + ventasDiarias[1]);
				break;
			case "miercoles":
				System.out.println("Las ventas del miercoles fueron por: " + ventasDiarias[2]);
				break;
			case "jueves":
				System.out.println("Las ventas del jueves fueron por: " + ventasDiarias[3]);
				break;
			case "viernes":
				System.out.println("Las ventas del viernes fueron por: " + ventasDiarias[4]);
				break;
			case "sabado":
				System.out.println("Las ventas del sabado fueron por: " + ventasDiarias[5]);
				break;
			case "domingo":
				System.out.println("Las ventas del domingo fueron por: " + ventasDiarias[6]);
				break;
			default:
				System.out.println("Esto no esta dentro de las opciones");
				break;
		}
	}
	public void PorPlatillo() {
		Scanner sc = new Scanner (System.in);
		int opcion;

		System.out.println("Escoja un platillo para ver las ventas");
		System.out.println(" 1. Platillo 1\n 2. Platillo 2\n 3. Platillo 3\n 4. Platillo 4\n 5. Platillo 5\n 6. Platillo 6\n 7. Platillo 7");

		if (opcion > 0 || opcion < 8) {
			System.out.println("- Platillo " + (opcion) + ": "  + ventasSemanalesPlatillo[opcion - 1]);
		} else {
			System.out.println("Esto no esta dentro de las opciones");
		}
	}
	public void DescuentoDeJubilado() {
		String dias [] = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};

		for(int i = 0; i < dias.length; i++) {
			System.out.println("El total de descuento de jubilado otorgado el " + dias[i] + "fue de " + jubiladoDiario[i]);
		}
		
	}
	public void PorPlatilloEnOferta() {
		String dias [] = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};

		for(int i = 0; i < dias.length; i++) {
			System.out.println("El total de descuento por platillo de oferta el " + dias[i] + "fue de " + ofertaDiario[i]);
		}
	}
}
