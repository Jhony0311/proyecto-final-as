package restaurante;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
	
public class Principal {
	private static Scanner sc;
	public double PrecioPlatillo[]=new double [7];
	public ArrayList <String> TomarPedidoStr = new ArrayList <String> ();
	public ArrayList <Integer> TomarPedidoCantidad = new ArrayList <Integer> (); 
	public ArrayList <Double> TomarPedidoPrecio = new ArrayList <Double> ();
	
	public int Cantidad;
	public int Opcion;
	public int encargado;
	private double Descuentojub;
	private double Descuento=0;
	private double DescuentoDia;
	private char jubilado;
	private double Subtotal=0;
	private double Total;
	private String Dia = null;
	
	private Ventas reportes = new Ventas();
	
	public void Menu() {
		System.out.println("--------------------Menu de platillos--------------------");
		System.out.println(" 1. Platillo 1\n 2. Platillo 2\n 3. Platillo 3\n 4. Platillo 4\n 5. Platillo 5\n 6. Platillo 6\n 7. Platillo 7");
	}
	public void VerMenuVentas() {
		Scanner sc=new Scanner (System.in);
		int Opcionv;
		System.out.println(" 1.Ventas semanales\n 2.Ventas diarias\n 3.Ventas diarias por encargado\n 4.Ventas por distribuci�n porcentual\n 5.Platillo m�s vendido\n 6.Platillo menos vendido\n 7.Ventas para un d�a especifico\n 8.Venta por platillo\n 9.Por platillo en oferta\n 10.Descuentos en jubilados");
		System.out.println("Escoja la opcion que desea ver");
		Opcionv=sc.nextInt();

		switch (Opcionv) {
		case 1:
			reportes.Semanal();
			break;
		case 2:
			reportes.Diarias();
			break;
		case 3:
			reportes.DiariasPorEncargado();
			break;
		case 4:
			reportes.PorDistribucionPorcentual();
			break;
		case 5:
			reportes.MasVendido();
			break;
		case 6:
			reportes.MenosVendido();
			break;
		case 7:
			reportes.DiaEspecifico();
			break;
		case 8:
			reportes.PorPlatillo();
			break;
		case 9:
			reportes.PorPlatilloEnOferta();
			break;
		case 10:
			reportes.DescuentoDeJubilado();
			break;
		default:
			System.out.println("Este valor no est� entra las opciones.");
			break;
		}
	}

	public void Configuracion(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Para empezar se requiere ingresar los precios de los platillos");
		for(int i=0;i<PrecioPlatillo.length;i++) {
			System.out.println("Ingrese el precio del platillo "+(i+1)+" :");
			PrecioPlatillo[i]=sc.nextDouble();
			
		}
	}
	
	public void TomarPedido() {
		Scanner sc=new Scanner (System.in);

		double sub = 0;
		double descuento = 0;
		
		switch (Opcion) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				System.out.println("Ingrese la cantidad de platillos que desea:");
				Cantidad=sc.nextInt();
				sub = PrecioPlatillo[Opcion - 1] * Cantidad;
				TomarPedidoCantidad.add(Cantidad);
				TomarPedidoPrecio.add(sub);
				descuento = DescuentoPorDia(Opcion, Cantidad);

				reportes.guardarVentaPlatillo(Opcion, sub, Cantidad, descuento, Dia, encargado);
				descuentoDia = descuentoDia + descuento;
				Subtotal = Subtotal + sub;
			default:
				System.out.println("Su opci�n no esta en el men�");
				break;
		}
	}
	
	public double DescuentoPorDia(opcion, precio) {
		double descuento = 0;

		if (opcion == 1 && Dia == "lunes") {
			descuento = precio * 0.2;
		} else if (opcion == 2 && Dia == "martes") {
			descuento = precio * 0.2;
		} else if (opcion == 3 && Dia == "miercoles") {
			descuento = precio * 0.2;
		} else if (opcion == 4 && Dia == "jueves") {
			descuento = precio * 0.2;
		} else if (opcion == 5 && Dia == "viernes") {
			descuento = precio * 0.2;
		} else if (opcion == 6 && Dia == "sabado") {
			descuento = precio * 0.2;
		} else if (opcion == 7 && Dia == "domingo") {
			descuento = precio * 0.2;
		}
		
		return descuento;
	}
	
	public double CalcularSubtotal() {
		switch (Opcion) {
		case 1:
			Subtotal=Subtotal+sub1;
			break;
		case 2:
			Subtotal=Subtotal+sub2;
			break;
		case 3:
			Subtotal=Subtotal+sub3;
			break;
		case 4:
			Subtotal=Subtotal+sub4;
			break;
		case 5:
			Subtotal=Subtotal+sub5;
			break;
		case 6:
			Subtotal=Subtotal+sub6;
			break;
		case 7:
			Subtotal=Subtotal+sub7;
		break;
		}
		return Subtotal;
		
	}
	
	public void DescuentoJubilado() {
		Scanner sc=new Scanner (System.in);

		System.out.println("�Es usted jubilado?(s/n)");
		jubilado=sc.next().charAt(0);

		if (jubilado=='s'||jubilado=='S') {
			Descuentojub=Subtotal*0.2;
			Total=(Subtotal-Descuentojub)-DescuentoDia;
			reportes.guardarDescuentoJubilado(Descuentojub, Dia);
		}
		else {
			Total=Subtotal-DescuentoDia;
		}
	}
	
	public void Factura() {
		System.out.println("CANTIDAD   DESCRIPCION   PRECIO");
		for (int i=0; i<TomarPedidoCantidad.size(); i++) {
			System.out.println(" "+(TomarPedidoCantidad.get(i))+ "         " +(TomarPedidoStr.get(i))+ "      "+ (TomarPedidoPrecio.get(i)));
		}

		if (jubilado=='s'||jubilado=='S') {
			System.out.println("Descuento de jubilado obtenido:       "+Descuentojub);
		}
		System.out.println("Descuento obtenido en el platillo del dia:      "+DescuentoDia);
		System.out.println("SUBTOTAL:          "+Subtotal);
		System.out.println("TOTAL:         "+Total);
		reportes.guardarVentaDiaria(Total, Dia, encargado);
	}

	public boolean agregarDia() {
		Scanner sc=new Scanner (System.in);
		System.out.println("Ingrese el dia de hoy (lunes, martes, miercoles...):");
		Dia=sc.nextLine();

		Dia = Dia.toLowerCase();

		switch (Dia) {
			case "lunes":
			case "martes":
			case "miercoles":
			case "jueves":
			case "viernes":
			case "sabado":
			case "domingo":
				return true;
				break;
			default:
				System.out.println("Esto no esta dentro de las opciones");
				return false;
				break;
		}
	}

	public boolean agregarEncargado() {
		Scanner sc=new Scanner (System.in);
		System.out.println("Ingrese numero del encargado (1, 2, 3)");
		encargado=sc.nextInt();

		if (encargado < 1 || encargado > 3 ) {
			return false;
		} else {
			return true;
		}
	}

	public void Caja() {
		Scanner sc=new Scanner (System.in);
		char respPedido;
		boolean diaCorrecto, encargadoCorrecto;
		do {
			diaCorrecto = agregarDia();
		} while (!diaCorrecto);

		do {
			encargadoCorrecto = agregarEncargado();
		} while (!diaCorrecto);
		
		do {
			System.out.println("Escoga una opcion del men�:");
			Opcion=sc.nextInt();
			TomarPedido();

			System.out.println("�Desea agregar algo mas a su orden? (s/n)");
			respPedido=sc.next().charAt(0);
		} while (respPedido=='s'||respPedido=='S');

		DescuentoJubilado();
		Factura();

		// Borrar variables
		Dia = null;
		encargado = null;
		TomarPedidoStr.clear();
		TomarPedidoCantidad.clear();
		TomarPedidoPrecio.clear();
	}

	public static void main (String args[]) throws IOException {
		char respuesta= 's';
		char respuesta1;
		
		Principal pri=new Principal ();
		pri.Configuracion();
		
		do {
			pri.Menu();
			pri.Caja();
			System.out.println("�Desea tomar otro pedido? (s/n)");
			respuesta=(char)System.in.read();
		} while (respuesta=='s'||respuesta=='S');
		
		do {
			System.out.println("--------------------Men� de ventas--------------------");
			pri.VerMenuVentas();
			System.out.println("�Desea ver otro reporte?(s/n)");
			respuesta1=(char)System.in.read();
		} while (respuesta1=='s'||respuesta1=='S');	
	}
}
